On démontre ici un fonctionnement de sujet de TP pensé par Bruno
BEAUFILS et Yvan PETER.

L'idée est de ne montrer que ce que les étudiants doivent faire à un
instant donné et de stocker la progression des étudiants dans le TP.

# Principe général

On utilise des classes et des ID sur les éléments HTML pour montrer ou
cacher des parties de l'énoncé via une simple règle CSS (classe
`invisible`).

Certains éléments HTML servent à exécuter les actions de modification
CSS via le déclenchement d'une fonction Javascript lors de l'évènement
`onclick`.

En attendant mieux un exemple de fonctionnement est dans le fichier
[`index.html`](index.html) qui contient le code javascript (avec une
documentation rudimentaire) en fin de fichier.

# Génération à partir de Markdonw via pandoc 

Pour simplifier l'édition de l'exercice on peut générer le code HTML à
partir d'un source Markdown et de pandoc (en utilisant l'extension
[`fenced_divs`](https://pandoc.org/MANUAL.html#divs-and-spans) qui est
activée par défaut par pandoc).

Pour exemple, le fichier [`exo.md`](exo.md) peut-être transformé en
[`exo.html`](exo.html) avec toute l'infrastructure qui va bien via la
commande :

```bash
pandoc --template=pandoc.html exo.md -o exo.html
```

Le fichier [`pandoc.html`](pandoc.html) est le modèle pandoc qui
permet de générer le fichier HTML (il intègre en gros l'entête et le
code javascript présent dans [`index.html`](index.html)).

Le fichier [`makefile`](makefile) contient une règle pour les
transformations de `.md` en `.html`.

# Stockage de l'avancement 

On a un mini-serveur en python qui permet de stocker les informations
que l'on veut :

- login de l'étudiant
- étapes franchies avec la date de franchissement

L'*API* REST est pour l'instant simplissime
  
- `/debuter/LOGIN` l'étudiant *`LOGIN`* démarre le TP
- `/LOGIN/avancer/ID` notifie le passage à une étape suivante `ID`
  pour l'étudiant `LOGIN`
- `/LOGIN/reculer/ID` notifie le passage à une étape précédente `ID`
  pour l'étudiant `LOGIN`

On a fait un truc simplissime en Python avec FastAPI. Le code est dans
[rest.py](rest.py).

Ça écrit une ligne pour chaque requête dans le fichier `./data`.

Sous Debian, pour que ça marche il faut installer les paquets
`uvicorn` et `python3-fastapi`. Pour démarrer le serveur il faut
exécuter la commande : `uvicorn rest:app --reload`.

Pour l'instant :

- la date de l'évènement n'est pas stockée ;
- on considère l'étape terminée quand on passe à la suivante.
    
# TODO

- [ ] Il faut modifier le serveur pour assurer l'accès concurrent
      correct au fichier `data`

- [ ] Ajouter la possibilité pour chaque étudiant de valider quand ils
      ont fait une étape.
      
  Dans le cas simple de comparaison d'une valeur ça peut se faire
  facilement avec un champ de formulaire (genre `textarea`) dans le
  code HTML et une comparaison de la valeur qu'elle contient.

- [ ] Il faut ajouter un mininum de sécurité
  
  Pour être vraiment propre (s'assurer qu'il n'y a pas de *triche* :
  quelqu'un qui avance pour un autre) il faudrait trouver un mécanisme
  simple qui permet d'identifier l'utilisateur de manière
  efficace. Mais AMHA pour ce que l'on avait en tête avec Yvan pour le
  moment ça n'est pas nécessaire.

- [ ] Corriger le serveur pour gérer le
      [CORS](https://developer.mozilla.org/fr/docs/Web/HTTP/CORS/Errors/CORSMissingAllowOrigin)
      lors de l'appel de `XMLHttpRequest`.
  
