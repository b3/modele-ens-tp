from typing import Union

from fastapi import FastAPI

app = FastAPI()

@app.get("/debuter/{nom}")
def read_root(nom: str):
    with open("./data", "a") as f:
        f.write(f"{nom}:demarre\n")

@app.get("/{nom}/avancer/{id}")
def read_root(nom: str, id: str):
    with open("./data", "a") as f:
        f.write(f"{nom}:suivant:{id}\n")

@app.get("/{nom}/reculer/{id}")
def read_root(nom: str, id: str):
    with open("./data", "a") as f:
        f.write(f"{nom}:precedent:{id}\n")
