all: exo.html

%.html: %.md
	pandoc --template=pandoc.html $< -o $@

clean:
	-rm *~
